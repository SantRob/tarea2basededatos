import java.sql.*;

public class Crud {
    private ConnectionSqlServer connection = new ConnectionSqlServer();
    public Crud() {

    }

    public void createSong(String songName, String songDate, String songArtist, String songGenre) {
        try {
            String sql = "INSERT INTO playList " +
                    "(name, launchDate, artist, genre) " +
                    "VALUES (?, ?, ?, ?)";

            PreparedStatement statement = connection.getConnection().prepareStatement(sql);
            statement.setString(1, songName);
            statement.setString(2, songDate);
            statement.setString(3, songArtist);
            statement.setString(4, songGenre);

            int rowsInserted = statement.executeUpdate();
            if (rowsInserted > 0) {
                System.out.println("Canción insertada correctamente");
            }

        } catch (Exception ex) {
            System.out.println(ex);
        }
    }

    public void listSong() {
        try {
            String sql = "SELECT IdSong, name, launchDate, artist, genre FROM playList";

            PreparedStatement statement = connection.getConnection().prepareStatement(sql);
            ResultSet resultSet = statement.executeQuery();

            System.out.println("\nMi playlist:");
            while (resultSet.next()) {
                int idSong = resultSet.getInt("IdSong");
                String name = resultSet.getString("name");
                String launchDate = resultSet.getString("launchDate");
                String artist = resultSet.getString("artist");
                String genre = resultSet.getString("genre");

                System.out.println("Id: " + idSong + ", Nombre: " + name + ", Fecha de lanzamiento: "
                        + launchDate + ", Artista: " + artist + ", Género: " + genre);
            }
        } catch (SQLException ex) {
            System.out.println(ex);
        }
    }

    public void updateSong(int idSong, String songName, String songDate, String songArtist, String songGenre) {
        try {
            String sql = "UPDATE playList SET " +
                    "name = ?, launchDate = ?, artist = ?, genre = ? " +
                    "WHERE IdSong = ?";

            PreparedStatement statement = connection.getConnection().prepareStatement(sql);
            statement.setString(1, songName);
            statement.setString(2, songDate);
            statement.setString(3, songArtist);
            statement.setString(4, songGenre);
            statement.setInt(5, idSong);

            int rowsInserted = statement.executeUpdate();
            if (rowsInserted > 0) {
                System.out.println("Canción actualizada correctamente");
            }

        } catch (Exception ex) {
            System.out.println(ex);
        }
    }

    public void deleteSong(int idSong) {
        try {
            String sql = "DELETE FROM playList " +
                    "WHERE IdSong = ?";

            PreparedStatement statement = connection.getConnection().prepareStatement(sql);
            statement.setInt(1, idSong);

            int rowsDeleted = statement.executeUpdate();
            if (rowsDeleted > 0) {
                System.out.println("Canción eliminada correctamente");
            }
        } catch (Exception ex) {
            System.out.println(ex);
        }
    }

    public boolean validSong(int idSong) {
        try {
            String sql = "SELECT name, launchDate, artist, genre FROM playList WHERE IdSong = " + idSong;

            PreparedStatement statement = connection.getConnection().prepareStatement(sql);
            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                return true;
            }
        } catch (SQLException ex) {
            System.err.println(ex);
        }
        System.err.println("Canción no existe en la base de datos");
        System.out.println();
        return false;
    }
}
