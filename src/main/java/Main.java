import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Crud crud = new Crud();
        Scanner scanner = new Scanner(System.in);

        boolean salir = false;
        while (!salir) {
            System.out.println();
            System.out.println("¡Bienvenido al menú de gestión de canciones!");
            System.out.println("1. Crear canción");
            System.out.println("2. Listar canciones");
            System.out.println("3. Actualizar canción");
            System.out.println("4. Eliminar canción");
            System.out.println("5. Salir");
            System.out.print("Por favor, ingresa tu elección: ");
            int opcion = scanner.nextInt();

            switch (opcion) {
                case 1:
                    crearCancion(scanner, crud);
                    break;
                case 2:
                    System.out.println("Listar canciones:");
                    crud.listSong();
                    break;
                case 3:
                    actualizarCancion(scanner, crud);
                    break;
                case 4:
                    eliminarCancion(scanner, crud);
                    break;
                case 5:
                    salir = true;
                    break;
                default:
                    System.out.println("Por favor, ingresa una opción válida.");
            }
        }
        scanner.close();
    }

    private static void crearCancion(Scanner scanner, Crud crud) {
        System.out.println("Crear canción:");
        System.out.print("Ingrese el nombre de la canción: ");
        String nombre = scanner.nextLine(); // Limpia el buffer del scanner
        nombre = scanner.nextLine(); // Lee la entrada del usuario
        System.out.print("Ingrese la fecha de lanzamiento (DD/MM/YYYY): ");
        String fechaLanzamiento = scanner.nextLine();
        System.out.print("Ingrese el nombre del artista: ");
        String artista = scanner.nextLine();
        System.out.print("Ingrese el género de la canción: ");
        String genero = scanner.nextLine();
        crud.createSong(nombre, fechaLanzamiento, artista, genero);
    }

    private static void actualizarCancion(Scanner scanner, Crud crud) {
        System.out.println("Actualizar canción:");
        System.out.print("Ingrese el ID de la canción a actualizar: ");
        int newId = scanner.nextInt();
        if (crud.validSong(newId)) {
            scanner.nextLine(); // Li9
            System.out.print("Ingrese el nuevo nombre de la canción: ");
            String newName = scanner.nextLine();
            System.out.print("Ingrese la nueva fecha de lanzamiento (DD/MM/YYYY): ");
            String newDate = scanner.nextLine();
            System.out.print("Ingrese el nuevo nombre del artista: ");
            String newArtist = scanner.nextLine();
            System.out.print("Ingrese el nuevo género de la canción: ");
            String newGenre = scanner.nextLine();
            crud.updateSong(newId, newName, newDate, newArtist, newGenre);
        }
    }

    private static void eliminarCancion(Scanner scanner, Crud crud) {
        System.out.println("Eliminar canción:");
        System.out.print("Ingrese el ID de la canción a eliminar: ");
        int idEliminar = scanner.nextInt();
        if (crud.validSong(idEliminar)){
            crud.deleteSong(idEliminar);
        }
    }
}
