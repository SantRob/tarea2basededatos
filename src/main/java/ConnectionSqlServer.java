import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionSqlServer {
    private Connection connection = null;

    public ConnectionSqlServer() {
        initialiceConection ();
    }

    private void initialiceConection() {
        String url = "jdbc:sqlserver://localhost:1433;databaseName=MUSIC";
        String user = "sa";
        String password = "Masterkey*";

        try {
            connection = DriverManager.getConnection(url, user, password);
            System.out.println("Conexión exitosa a la base de datos SQL Server");
        } catch (SQLException e) {
            System.err.println("Error al conectar a la base de datos: " + e.getMessage());
        }
    }

    public Connection getConnection() {
        return connection;
    }
}
